<?php

use App\User;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('questions', 'QuestionController@getAllQuestions');
Route::get('questions/{id}', 'QuestionController@getQuestion');
Route::post('questions', 'QuestionController@createQuestion');
Route::put('questions/{id}', 'QuestionController@updateQuestion');
Route::delete('questions/{id}','QuestionController@deleteQuestion');

Route::get('answers', 'AnswerController@getAllAnswers');
Route::get('answers/{id}', 'AnswerController@getAnswer');
Route::post('answers', 'AnswerController@createAnswer');
Route::put('answers/{id}', 'AnswerController@updateAnswer');
Route::delete('answers/{id}','AnswerController@deleteAnswer');

Route::get('categories', 'CategoryController@getAllCategories');
Route::get('categories/{id}', 'CategoryController@getCategory');
Route::post('categories', 'CategoryController@createCategory');
Route::put('categories/{id}', 'CategoryController@updateCategory');
Route::delete('categories/{id}','CategoryController@deleteCategory');

Route::get('userdata', 'UserDataController@getAllUserData');
Route::get('userdata/{id}', 'UserDataController@getSingleUser');
Route::post('userdata', 'UserDataController@createUser');









