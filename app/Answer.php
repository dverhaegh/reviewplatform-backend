<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answers';
    protected $fillable = ['answer', 'question_id']; 
    protected $hidden = ['created_at','updated_at']; 


    public function questions()
    {
        return $this->belongsTo(Question::class, 'id', 'question_id');
    }
}
