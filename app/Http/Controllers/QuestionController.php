<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Category;


class QuestionController extends Controller
{

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS FOR QUESTIONS/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public function getAllQuestions() {
    $questions = Question::get()->toJson(JSON_PRETTY_PRINT);
    return response($questions, 200);    
  }

public function getEverything(){
    $questions = Question::all();
    foreach($questions as $question){
      $question->answers = Answer::where('question_id', $question->id)
      ->get();
    }
    return response($questions, 200);
}
  
public function createQuestion(Request $request) {
    $question = new Question;
    $question->question = $request->question;
    $categoryList = Category::orderBy('created_at', 'desc')->first();
    $question->category_id = $categoryList->id;
    $question->save();
    return response($question, 200); 
    return response()->json([
        "message" => "Question record created"
    ], 201);

    
}
  
  public function getQuestion($id) {
    if (Question::where('id', $id)->exists()) {
        $question = Question::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
        return response($question, 200);
      } else {
        return response()->json([
          "message" => "Question not found"
        ], 404);
      }
  }

public function updateQuestion(Request $request, $id) {
  if (Question::where('id', $id)->exists()) {
      $question = Question::find($id);
      $question->question = is_null($request->question) ? $question->question : $request->question;
      $question->save();

      return response($question, 200); 
      return response()->json([
          "message" => "records updated successfully"
      ], 200, $questions);
      } else {
        return response()->json([
           "message" => "Question not found"
      ], 404);          
  }
}

public function deleteQuestion($id) {
    Question::destroy($id);
      return response()->json([
      "message" => "records deleted"
  ], 202);
      
  }

}
