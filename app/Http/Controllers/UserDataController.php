<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserData;

class UserDataController extends Controller
{
    public function getAllUserData() {
        $users = UserData::get()->toJson(JSON_PRETTY_PRINT);
        return response($users, 200);    
    
      }

    public function createUser(Request $request) {
        $user = new UserData;
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->email = $request->email;
        $user->place = $request->place;
        $user->anonymous = $request->anonymous;
        $user->save();

        return response($user, 200); 
        return response()->json([
            "message" => "User record created"
        ], 201);
        }

    public function getSingleUser($id) {
        if (UserData::where('id', $id)->exists()) {
            $user = UserData::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
                return response($user, 200);
            } else {
                return response()->json([
                  "message" => "User not found"
                ], 404);
              }
        }    
}
