<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS FOR CATEGORIES/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public function getAllCategories() {
    $categories = Category::get()->toJson(JSON_PRETTY_PRINT);
    return response($categories, 200);    
  }
  
  public function createCategory(Request $request) {
    $category = new Category;
    $category->category = $request->category;
    $category->save();
  
    return response($category, 200); 
    return response()->json([
        "message" => "Category record created"
    ], 201);
    }
  
  public function getCategory($id) {
    if (Category::where('id', $id)->exists()) {
        $category = Category::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
        return response($category, 200);
      } else {
        return response()->json([
          "message" => "Category not found"
        ], 404);
      }
  }
  
  public function updateCategory(Request $request, $id) {
  if (Category::where('id', $id)->exists()) {
      $category = Category::find($id);
      $category->category = is_null($request->category) ? $category->category : $request->category;
      $category->save();
  
      return response()->json([
          "message" => "records updated successfully"
      ], 200, $categories);
      } else {
        return response()->json([
           "message" => "Category not found"
      ], 404);          
  }
  }
  
  public function deleteCategory($id) {
  Category::destroy($id);
      return response()->json([
        "message" => "records deleted"
      ], 202);
      
  }
  
  public function QuestionAnswer($questionid)
  {       
     $question = Question::where("id", $questionid)->get();
     $answers = Answer::all();
  
     $question->answers()->attach($answers);
      
      dd($question->answers);
  }

}
