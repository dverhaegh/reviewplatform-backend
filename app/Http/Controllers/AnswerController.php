<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;
use App\Question;


class AnswerController extends Controller
{

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS FOR ANSWERS/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public function getAllAnswers() {
    $answers = Answer::get()->toJson(JSON_PRETTY_PRINT);
    return response($answers, 200);    
  }

  public function createAnswer(Request $request) {
    $answer = new Answer;
    $answer->answer = $request->answer;
    $questionList = Question::orderBy('created_at', 'desc')->first();
    $answer->question_id = $questionList->id;
    $answer->save();
  
    return response($answer, 200); 
    return response()->json([
        "message" => "Answer record created"
    ], 201);
    }

  public function getAnswer($id) {
      if (Answer::where('id', $id)->exists()) {
          $answer = Answer::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
          return response($answer, 200);
        } else {
          return response()->json([
            "message" => "Answer not found"
          ], 404);
      }
  }
  
  public function updateAnswer(Request $request, $id) {
      if (Answer::where('id', $id)->exists()) {
          $answer = Answer::find($id);
          $answer->answer = is_null($request->answer) ? $answer->answer : $request->answer;
          $answer->save();
    
          return response()->json([
              "message" => "records updated successfully"
          ], 200);
          } else {
            return response()->json([
              "message" => "Question not found"
          ], 404);          
      }
  }

  public function deleteAnswer($id) {
      if(Answer::where('id', $id)->exists()) {
          $answer = Answer::find($id);
          $answer->delete();
    
          return response()->json([
            "message" => "records deleted"
          ], 202);
        } else {
          return response()->json([
            "message" => "Question not found"
          ], 404);
      }
  }
}
