<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';
    protected $fillable = ['question', 'category_id']; 
    protected $hidden = ['created_at','updated_at']; 

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }


    public function categories()
    {
        return $this->belongsTo(Category::class);
    }
}
