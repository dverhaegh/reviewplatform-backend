<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
    protected $table = 'userdata';
    protected $fillable = ['gender','name','email','place','anonymous']; 
    protected $hidden = ['created_at','updated_at']; 
}
